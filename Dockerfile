# Download python specified version image from Docker Hub
FROM python:3.8-slim-buster

# Create a work directory named demoapp
WORKDIR /demoapp

# Copy all contens of the current directory to the demoapp directory
COPY . /demoapp

# Install requirements
RUN pip install -r requirements.txt

# RUN the application inside the container at port 8000
CMD [ "python3" , "manage.py" , "runserver", "0.0.0.0:8000" ]
